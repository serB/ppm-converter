#ifndef EXPORT
#define EXPORT

#include "initialisation.h"


/**
 * @brief Crée un fichier (pbm ou pgm), à partir de la structure de données
 * passée en paramètre.
 *
 * @param donnees
 * @param name
 */
void export_image(format_t,char*);

#endif
