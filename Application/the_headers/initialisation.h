#ifndef INITIALISATION
#define INITIALISATION

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define DECAL_ROUGE 32
#define DECAL_VERT 16

/**
 * @enum t_image
 *
 * @brief Type énuméré qui représente les 3 types de fichiers (ppm, pgm et pbm)
 *
 */
typedef enum type_image{
  P1=1, P2=2, P3=3
}t_image;

/**
 * @struct format_t
 *
 * @brief La structure de donnée qui représente un fichier (ppm, pgm ou pbm).
 * Avec son type, sa largeur et hauteur, la valeur maximale des pixels, et un tableau contenant
 * tous les pixels.
 *
 */
typedef struct {
  t_image type;
  uint32_t largeur;
  uint32_t hauteur;
  uint16_t val_max;
  uint64_t* tab;
}format_t;

/**
 * @brief Lit un fichier P3(ppm) et retourne une structure de données
 * qui représente ce fichier.
 *
 * @param f
 * @return format_t
 */
format_t lire_fichier(FILE *f);

/**
 * @brief Stocke 3 entiers (16 bits) dans un entier sur 64 bits.
 *
 * @param r
 * @param v
 * @param b
 * @return uint64_t
 */
uint64_t P3_pixel(int r, int v, int b);

#endif
