#ifndef TRANSFORMATION
#define TRANSFORMATION

#include "initialisation.h"
#include <stdlib.h>

#define CONST_A 0.299
#define CONST_B 0.587
#define CONST_C 0.114
#define ALPHA 0.5

/**
 * @brief Convertit une structure de données représentant un fichier ppm, en une
 * structure de données représentant un fichier pgm. Selon des coefficients a, b et c.
 * (avec a+b+c=1)
 *
 * @param donnees
 * @param a un float entre 0 et 1 qui gère la sensibilité du rouge
 * @param b un float entre 0 et 1 qui gère la sensibilité du vert
 * @param c un float entre 0 et 1 qui gère la sensibilité du bleu
 * @return format_t
 */
format_t PPM_to_PGM(format_t,float a, float b, float c);

 /**
  * @brief Convertit une structure de données représentant un fichier ppm, en une
  * structure de données représentant un fichier pbm. Selon un coefficient alpha
  *
  * @param donnees
  * @param alpha un float entre 0 et 1, qui gère la sensibilité de la binarisation
  * @return format_t
  */
format_t PPM_to_PBM(format_t,float alpha);

/**
 * @brief Récupère la valeur bleue, encodée dans un entier 64 bits
 *
 * @param couleurs
 * @return uint16_t
 */
uint16_t get_bleu(uint64_t);

/**
 * @brief Récupère la valeur rouge d'un pixel, encodée dans un entier 64 bits
 *
 * @param couleurs
 * @return uint16_t
 */
uint16_t get_rouge(uint64_t);

/**
 * @brief Récupère la valeur verte, encodée dans un entier 64 bits
 *
 * @param couleurs
 * @return uint16_t
 */
uint16_t get_vert(uint64_t);

#endif
