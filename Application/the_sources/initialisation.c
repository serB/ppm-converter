/**
 * @brief Types et fonctions permettant de lire un fichier .ppm et de
 * stocker les informations dans une structure de données.
 *
 * @file initialisation.c
 * @author BEAUGRAND Élisa - BRÈS Maxence
 * @date 2018
 */

#include "initialisation.h"

uint64_t P3_pixel(int r, int v, int b){
  uint64_t rouge = r;
  rouge = rouge << DECAL_ROUGE;
  uint64_t vert = v;
  vert = vert << DECAL_VERT;
  uint64_t bleu = b;

  return (rouge|vert|bleu);
}

format_t lire_fichier(FILE* f){
  format_t donnees;
  int type,r,v,b;
  int i=0;
  char lettre;
  fscanf(f,"%c %i", &lettre, &type);
  // On vérifie que le fichier est bien un P3 (ppm)
  if(lettre != 'P' && type != P3){
    fprintf(stderr,"Le fichier à lire n'est pas un PPM\n");
    exit(1);
  }
  donnees.type = P3;
  fscanf(f,"%i %i %hi", &donnees.largeur, &donnees.hauteur, &donnees.val_max);
  donnees.tab = malloc(sizeof(int64_t)*donnees.hauteur*donnees.largeur);
  // On lit la valeur des pixels jusqu'à la fin du fichier
  while(!feof(f)){
    fscanf(f,"%i %i %i", &r, &v, &b);
    donnees.tab[i]=P3_pixel(r,v,b);
    i++;
  }
  return donnees;
}
