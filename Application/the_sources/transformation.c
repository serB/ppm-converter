/**
 * @brief Fonctions permettant de transformer une structure ppm, en un pbm ou un pgm.
 *
 * @file transformation.c
 * @author BEAUGRAND Élisa - BRÈS Maxence
 * @date 2018
 */

#include "initialisation.h"
#include "transformation.h"
#include <stdint.h>

format_t PPM_to_PGM(format_t donnees, float a, float b, float c){
  // On vérifie que le fichier en entrée est bien un ppm
  if(donnees.type != P3){
    fprintf(stderr,"Conversion à partir du mauvais type de fichier\n");
    exit(2);
  }
  format_t new_donnees;
  uint16_t rouge,vert,bleu;
  new_donnees.type = P2;
  new_donnees.hauteur = donnees.hauteur;
  new_donnees.largeur = donnees.largeur;
  new_donnees.val_max = donnees.val_max;
  // On alloue un tableau pour stocker la nouvelle valeur des pixels
  new_donnees.tab = malloc(sizeof(int64_t)*new_donnees.hauteur*new_donnees.largeur);
  // On calcule la valeur de tous les nouveaux pixels
  for(int i=0;i<new_donnees.largeur*new_donnees.hauteur;i++){
    rouge = (donnees.tab[i] >> 32) & 0xFFFF;
    vert = (donnees.tab[i] >> 16) & 0xFFFF;
    bleu = donnees.tab[i] & 0xFFFF;
    new_donnees.tab[i] = a * rouge + b * vert + c * bleu;
  }
  return new_donnees;
}

format_t PPM_to_PBM(format_t donnees, float alpha){
  // On vérifie que le fichier en entrée est bien un ppm
  if(donnees.type != P3){
    fprintf(stderr,"Conversion à partir du mauvais type de fichier\n");
    exit(3);
  }
  format_t new_donnees;
  uint16_t rouge,vert,bleu;
  float temp;
  new_donnees.type = P1;
  new_donnees.hauteur = donnees.hauteur;
  new_donnees.largeur = donnees.largeur;
  new_donnees.val_max = donnees.val_max;
  // On alloue un tableau pour stocker la nouvelle valeur des pixels
  new_donnees.tab = malloc(sizeof(int64_t)*new_donnees.hauteur*new_donnees.largeur);
  float diviseur = new_donnees.val_max*new_donnees.val_max*new_donnees.val_max;
  // On calcule la valeur de tous les nouveaux pixels
  for(int i=0;i<new_donnees.largeur*new_donnees.hauteur;i++){
    rouge = get_rouge(donnees.tab[i]);
    vert = get_vert(donnees.tab[i]);
    bleu = get_bleu(donnees.tab[i]);
    temp = (rouge * vert * bleu) / diviseur;
    if(temp >= alpha)
      new_donnees.tab[i] = 0;
    else
      new_donnees.tab[i] = 1;
  }
  return new_donnees;
}

uint16_t get_rouge(uint64_t couleurs){
  return (couleurs >> DECAL_ROUGE) & 0xFFFF;
}

uint16_t get_vert(uint64_t couleurs){
  return (couleurs >> DECAL_VERT) & 0xFFFF;
}

uint16_t get_bleu(uint64_t couleurs){
  return couleurs & 0xFFFF;
}
