/**
 * @brief Transforme une image ppm (passée par fichier ou par l'entrée standard), en une
 * image pbm ou pgm.
 *
 * @file main.c
 * @author BEAUGRAND Élisa - BRÈS Maxence
 * @date 2018
 */

#include "initialisation.h"
#include "export.h"
#include "transformation.h"
#include <string.h>

FILE* options(int* tab_options, int nb_arg,char *arg[],char** nom_fichier){
  //Cette fonction remplit le tableau de booleen tab_options.
  //Si un des arguments est un nom de fichier, tab_options[0]=1 sinon tab_options[0]=0
  //Si un des arguments est -g, tab_options[1]=1 sinon tab_options[1]=0
  //Si un des arguments est -b, tab_options[2]=1 sinon tab_options[2]=0
  FILE *f=NULL;
  if (nb_arg < 2 || nb_arg > 4){
    fprintf(stderr,"Format : ./transformer -g et/ou -b (image.ppm)\nLes arguments peuvent être mis dans n'importe quel ordre.\n");
    exit(1);
  }
  for(int i=1; i<nb_arg; i++){

    if (arg[i][0]=='-'){
      if (!(strcmp("-g",arg[i])))
        tab_options[1]=1;
      else if (!(strcmp("-b",arg[i])))
        tab_options[2]=1;
      else if (!(strcmp("-h",arg[i]))){
        fprintf(stderr,"Format : ./transformer -g et/ou -b (image.ppm)\nLes arguments peuvent être mis dans n'importe quel ordre.\n");
        exit(0);
      }
    }else{
      if((f = fopen(arg[i],"r")) != NULL){
        tab_options[0]=1;
        if(strstr(arg[i],".ppm") == NULL){
          fprintf(stderr,"Le fichier en paramètre n'est pas un .ppm");
          exit(1);
        }
        *nom_fichier = malloc(strlen(arg[i]-4));
        strncpy(*nom_fichier,arg[i],strlen(arg[i])-4);
      }
    }
  }
  return f;
}


int main(int argc,char *argv[]){

  // Initialisation, on récupère les arguments

  char* nom_fichier;
  int tab_options[3] = {0,0,0};
  FILE *f=options(tab_options,argc,argv,&nom_fichier);


  //S'il n'y avait pas de fichier en argument, ou si le fichier était invalide, on passe sur une lecture console
  if (!tab_options[0]){
    f=stdin;
    nom_fichier="image";
  }

  format_t image = lire_fichier(f);

  // Si aucune option n'a été choisie on affiche l'aide
  if(!tab_options[1] && !tab_options[2]){
    fprintf(stderr,"Format : ./transformer -g et/ou -b (image.ppm)\nLes arguments peuvent être mis dans n'importe quel ordre.\n");
    exit(1);
  }

  // transformation

  //Si un passage en niveau de gris est demandé
  if(tab_options[1]){
    format_t image_pgm = PPM_to_PGM(image,CONST_A,CONST_B,CONST_C);
    export_image(image_pgm,nom_fichier);
  }

  //Si un passage en niveau de gris est demandé
  if(tab_options[2]){
    format_t image_pbm = PPM_to_PBM(image,ALPHA);
    export_image(image_pbm,nom_fichier);
  }

  return 0;
}
