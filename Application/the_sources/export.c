/**
 * @brief Fonctions permettant de créer un fichier pgm ou pbm, à partir d'une structure.
 *
 * @file export.c
 * @author BEAUGRAND Élisa - BRÈS Maxence
 * @date 2018
 */
#include "export.h"
#include <string.h>

void export_image(format_t donnees, char *name){
  FILE *f;
  char* extension;

  if(donnees.type == P1){extension = ".pbm";}
  if(donnees.type == P2){extension = ".pgm";}
  // On crée le nom du fichier
  char* nom = malloc(sizeof(char)*(strlen(name)+strlen(extension)));
  strcat(nom,name);
  strcat(nom,extension);
  f = fopen(nom,"w"); // Avec l'option w le fichier est effacé avant écriture

  // On commence à écrire les données dans le fichier
  // Type de fichier
  fprintf(f,"P%i\n",donnees.type);
  // Largeur et Hauteur
  fprintf(f,"%i %i\n",donnees.largeur,donnees.hauteur);
  // Valeur maximale dans le cas d'un PGM
  if(donnees.type==P2)
    fprintf(f,"%i\n",donnees.val_max);
  // Les données de l'image
  for(int i=0;i<(donnees.largeur*donnees.hauteur);i++){
      if(i==donnees.largeur)
        fprintf(f,"\n");
      fprintf(f,"%li ",donnees.tab[i]);
  }
  return;
}
